//Assignment 2 Playing Card
//Devon Norman

#include <iostream>
#include <conio.h>

using namespace std;


//Suit listing enum
enum Suit
{

	Clubs,
	Spades,
	Diamonds,
	Hearts
	
};


//Rank Listing Enum
enum Rank
{  //rank 1-14
	Two = 2,  //1 Lowest
	Three = 3, //2
	Four = 4, //3
	Five = 5, //4
	Six = 6, //5
	Seven = 7, //6
	Eight = 8, //7            //Definition isn't exactly required, it's up to judgement.
	Nine = 9, //8
	Ten = 10, //9
	Jack = 11, //10
	Queen = 12, //11
	King = 13, //12
	Ace = 14, //Highest
};

//Structure of cards
struct Card
{
	Rank rank;
	Suit suit;
};

void PrintCard(Card card)
{
	string rank;
	switch (card.rank)
	{
	case Two:
		rank = "Two";
		break;
	case Three:
		rank = "Three";
		break;
	case Four:
		rank = "Four";
		break;
	case Five:
		rank = "Five";
		break;
	case Six:
		rank = "Six";
		break;
	case Seven:
		rank = "Seven";
		break;
	case Eight:
		rank = "Eight";
		break;
	case Nine:
		rank = "Nine";
		break;
	case Ten:
		rank = "Ten";
		break;
	case Jack:
		rank = "Jack";
		break;
	case Queen:
		rank = "Queen";
		break;
	case King:
		rank = "King";
		break;
	case Ace:
		rank = "Ace";
		break;
	}
	string suit;
	switch (card.suit)
	{
	case Clubs:
		suit = "Clubs";
	case Spades:
		suit = "Spades";
	case Diamonds:
		suit = "Diamonds";
	case Hearts:
		suit = "Hearts";
	}
	cout << "The " << rank << " of " << suit << '\n';
}

Card HighCard(Card card1, Card card2)
{
	return card1.rank >= card2.rank ? card1 : card2;
}

int main()
{
	//Card A
	Card a;
	a.rank = Jack;
	a.suit = Hearts;

	//Card B
	Card b;
	b.rank = Ace;
	b.suit = Spades;

	PrintCard(a);
	PrintCard(b);
	PrintCard(HighCard(a, b));


	(void)_getch();
	return 0;
}

